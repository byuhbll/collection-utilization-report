#!/bin/bash
####################################################################################################
# Calculates the utilization levels of items within each of the ScholarSearch "Collection" facets, #
# and returns a summary of utilization as a CSV.  This report should be run on a Symphony server.  #
####################################################################################################

# -------------------
# Global declarations
# -------------------

# The Symphony libraries to be considered.
included_libraries="LEE,LEE-LRC,MUSIC"

# The home locations which allow for circulation.
inhouse_locations="~BEAN-F,BEAN-Q,JERUSALEM,JERUSLM-GC,JERUSLM-Q,JORDAN,LONDON,SLC-BKS,SLC-CALC,SLC-CASS,SLC-CDP,SLC-COMP,SLC-FPROJ,SLC-ROOM,SLC-TVVCR,SLC-VID,SWKT-LOAN,MICROCARD,MICROFICHE,MICROFORM,MICROPRINT"
circable_locations=`git archive --remote=ssh://git@bitbucket.org/byuhbll/yellow HEAD:src/main/mapping availability.tsv | tar -xm --to-stdout | python3 availability-to-location.py circulatable --delimiter=","`

# Base selitem command used as in-common by several other functions. 
selitem="selitem -iC -oB -y\"$included_libraries\""

# ---------
# Functions
# ---------

# Returns the number of distinct lines in the input.
#
# @param $1 the data to be sorted, uniqued, and counted
function countDistinct {
    echo "$1" | sort -u | wc -l
}

# Given a set of catalog keys, retrieves the corresponding items which are eligible for circulation.
#
# @param $1 the list of catalog keys to query against
function canCircItems {
    local cmd="echo \"$1\" | $selitem -l\"$circable_locations\""
    echo "$cmd" | sudo -iu sirsi
}

# Given a set of catalog keys, retrieves the corresponding items which have circulated at least once
# in the Symphony ILS.
#
# @param $1 the list of catalog keys to query against
function didCircItems {
    local cmd="echo \"$1\" | $selitem -l\"$circable_locations\" -A\">0\""
    echo "$cmd" | sudo -iu sirsi
}

# Given a set of catalog keys, retrieves the corresponding items which are eligible for in-house
# usage.
#
# @param $1 the list of catalog keys to query against
function canInHouseItems {
    local cmd="echo \"$1\" | $selitem -l\"$inhouse_locations\""
    echo "$cmd" | sudo -iu sirsi
}

# Given a set of catalog keys, retrieves the corresponding items which have had at least 1 in-house
# usage in the Symphony ILS.
#
# @param $1 the list of catalog keys to query against
function didInHouseItems {
    local cmd="echo \"$1\" | $selitem -l\"$inhouse_locations\" -j\">0\""
    echo "$cmd" | sudo -iu sirsi
}

# Retrieves the list of catalog keys belonging to a given ScholarSearch collection.
#
# @param $1 the collection identifier
function catkeysInCollection {
    local cmd="curl \"http://localhost:8983/solr/byu/select?fl=sourceRecordId&fq={%21collapse%20field=sourceRecordId}&indent=on&wt=csv&csv.header=false&rows=10000000&q=collection_ss:%22lee%20--%20$1%22\""
    ssh k3.lib.byu.edu "$cmd" | sort -u
}

function allCatkeys {
    local cmd="selcatalog -oC"
    echo "$cmd" | sudo -iu sirsi
}

function calcUsageForKeys {
    local collection=$1
    local keys=$2
    canCircItems=`canCircItems "$keys"`
    canInHouseItems=`canInHouseItems "$keys"`
    didCircItems=`didCircItems "$keys"`
    didInHouseItems=`didInHouseItems "$keys"`
    allEligibleItems="$canCircItems\n$canInHouseItems"
    allUsedItems="$didCircItems\n$didInHouseItems"
    echo "$collection,\
`countDistinct \"$allEligibleItems\"`,\
`countDistinct \"$allUsedItems\"`,\
`countDistinct \"$canCircItems\"`,\
`countDistinct \"$didCircItems\"`,\
`countDistinct \"$canInHouseItems\"`,\
`countDistinct \"$didInHouseItems\"`"
}


# ------------------------
# Main/global instructions
# ------------------------
echo "collection,all eligible items,all items used,items eligible for circulation,items circulated,items eligible for in-house use,items used in-house"
#allKeys=`allCatkeys`
#calcUsageForKeys all "$allKeys"
for collection in asian general govdocs humanities juvenile maps media_center music periodicals popular religion_famhist science social_science; do
    keys=`catkeysInCollection $collection`
    calcUsageForKeys $collection "$keys"
done

