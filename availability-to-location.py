"""
Reads the tab-delimited availability.tsv file used by ScholarSearch from STDIN and prints a list of
circulatable or usable (in-house) Symphony location codes.
"""
import sys
import csv
import argparse

ELIGIBLE_LIBRARIES = ["LEE", "LEE-LRC", "MUSIC"]
STATUSES = {
    "usable" : ["available_for_checkout", "available_only_in_library"],
    "circulatable" : ["available_for_checkout"]
}
VALID_TYPES = " or ".join(STATUSES.keys());
LOCATIONS_TO_IGNORE = [
        "ANTHRO-MUS", "ANTHRO-Q",
        "MICROCARD", "MICROFICHE", "MICROFORM", "MICROPRINT", # removed per Mike Hunter
        "JERUSALEM", "JERUSLM-GC", "JERUSLM-Q", "JORDAN" # removed per Jennifer Paustenbaugh
]
DEFAULT_DELIMITER = "\n"

LIBRARY_INDEX = 1
SYMPHONY_LOCATION_INDEX = 2
AVAILABILITY_STATUS_INDEX = 6

def main(status_type, delimiter = DEFAULT_DELIMITER):
    for row in csv.reader(iter(sys.stdin.readline, ''), delimiter='\t'):
        location = row[SYMPHONY_LOCATION_INDEX]
        if(row[LIBRARY_INDEX] in ELIGIBLE_LIBRARIES and
                row[AVAILABILITY_STATUS_INDEX] in STATUSES.get(status_type, []) and
                location.strip() and # Don't include rows with blank locations
                location not in LOCATIONS_TO_IGNORE):
            print(row[SYMPHONY_LOCATION_INDEX], end=delimiter)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("status_type", help="defines what type of locations to include; must be one of the"
            " following: " + VALID_TYPES)
    parser.add_argument("-d", "--delimiter", help="defines a custom delimiter for the output", 
            default=DEFAULT_DELIMITER)
    args = parser.parse_args()
    
    if(args.status_type not in STATUSES):
        sys.exit("required argument must be in " + VALID_TYPES)
    main(args.status_type, args.delimiter)
