#!/bin/sh
####################################################################################################
# Takes a line-delimited list of locations and prints the location code and size of the collection #
# in the following format: code (size)                                                             #
####################################################################################################

# The Symphony libraries to be considered.
included_libraries="LEE,LEE-LRC,MUSIC"

while read location; do
    cmd="selitem -l\"$location\" -y\"$included_libraries\" -oK | wc -l"
    locationSize=`echo "$cmd" | sudo -iu sirsi`
    echo "$location ($locationSize)"
done
